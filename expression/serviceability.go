package expression

//Serviceability Represent serviceability structure contains serviceability name and value
type Serviceability struct {
	*Entities
	Availabilities      map[string]string
	ServiceAbilityItems map[string]map[string]map[string]byte
}

// NewServiceability creates a new instance of the serviceability struct and returns a pointer to it.
func NewServiceability(entities *Entities, availabilities map[string]string, serviceAbilityItems map[string]map[string]map[string]byte) *Serviceability {
	return &Serviceability{Entities: entities, Availabilities: availabilities, ServiceAbilityItems: serviceAbilityItems}
}

//HasAvailability to check matching availability object in request
func (c *Serviceability) HasAvailability(name string, value string) bool {
	if val, ok := c.Availabilities[name]; ok && val == value {
		return true
	}
	return false
}

//HasItems to check matching serviceability itemtype name values
func (c *Serviceability) HasItems(itemType string, name string, values ...string) bool {
	for _, val := range values {
		if c.ServiceAbilityItems[itemType] != nil && c.ServiceAbilityItems[itemType][name] != nil {
			if _, ok := c.ServiceAbilityItems[itemType][name][val]; ok {
				return true
			}
		}
	}
	return false
}
