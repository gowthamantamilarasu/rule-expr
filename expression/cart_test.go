package expression

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var (
	testCategory            = "TestCategory"
	testOtherCategory       = "TestOtherCategory"
	testProcessContext      = "TestProcessContext"
	testOtherProcessContext = "TestOtherProcessContext"
	testProduct             = "TestProduct"
	testOtherProduct        = "TestOtherProduct"
	Categories              = map[string]map[string]byte{

		"TestCategory": {
			"cat003": 0,
		},
		"cat003": {},
	}
)

func TestCart_OfferContainerIsOfCategory(t *testing.T) {
	container := &Container{Category: testCategory}
	cart := cart([]*Container{container})
	cart.CurrentIteratingContainer = container

	assert.True(t, cart.OfferContainerIsOfCategory(testCategory, testOtherCategory))
	assert.False(t, cart.OfferContainerIsOfCategory(testOtherCategory))
}

func TestCart_OfferContainerIsOfProcessContext(t *testing.T) {
	container := &Container{ProcessContext: testProcessContext}
	cart := cart([]*Container{container})
	cart.CurrentIteratingContainer = container

	assert.True(t, cart.OfferContainerIsOfProcessContext(testProcessContext, testOtherProcessContext))
	assert.False(t, cart.OfferContainerIsOfProcessContext(testOtherProcessContext))
}

func TestCart_OfferContainerContainsProductOffering(t *testing.T) {
	container := &Container{
		Items: []*CartItem{
			{
				ID: testProduct,
			},
		},
	}
	cart := cart([]*Container{container})
	cart.CurrentIteratingContainer = container

	assert.True(t, cart.OfferContainerContainsProductOffering(testProduct))
	assert.False(t, cart.OfferContainerContainsProductOffering(testOtherProduct))
}

func TestCart_ContainsOfferContainerOfCategory(t *testing.T) {
	cart := cart([]*Container{
		{
			Category: testCategory,
		},
		{
			Category: testOtherCategory,
		},
	})

	assert.True(t, cart.ContainsOfferContainerOfCategory(testCategory))
	assert.True(t, cart.ContainsOfferContainerOfCategory(testOtherCategory))
	assert.False(t, cart.ContainsOfferContainerOfCategory("i-dont-exist"))
}

func TestCart_ContainProductOffering(t *testing.T) {
	cart := cart([]*Container{
		{
			Category: testCategory,
			Items: []*CartItem{
				{
					ID: testProduct,
				},
			},
		},
		{
			Category: testOtherCategory,
			Items: []*CartItem{
				{
					ID: testOtherProduct,
				},
			},
		},
	})

	assert.True(t, cart.ContainsProductOffering(testProduct))
	assert.True(t, cart.ContainsProductOffering(testOtherProduct))
	assert.True(t, cart.ContainsProductOffering(testProduct, testOtherProduct))
	assert.False(t, cart.ContainsProductOffering("i-dont-exist"))
}

func TestCart_ContainsProductOfferingInCategory(t *testing.T) {
	cart := cart([]*Container{
		{
			Category: testCategory,
			Items: []*CartItem{
				{
					Category: testCategory,
				},
			},
		},
		{
			Category: testOtherCategory,
			Items: []*CartItem{
				{
					Category: testOtherCategory,
				},
			},
		},
	})
	cart.Categories = Categories
	assert.True(t, cart.ContainsProductOfferingInCategory(testCategory))
	assert.True(t, cart.ContainsProductOfferingInCategory(testOtherCategory))
	assert.True(t, cart.ContainsProductOfferingInCategory("cat003"))
	assert.True(t, cart.ContainsProductOfferingInCategory(testCategory, "i-dont-exist"))
	assert.False(t, cart.ContainsProductOfferingInCategory("i-dont-exist"))
}

func TestCart_OfferContainerContainsProductOfferingInCategory(t *testing.T) {
	container := &Container{
		Items: []*CartItem{
			{
				Category: testCategory,
			},
		},
	}
	cart := cart([]*Container{container})
	cart.CurrentIteratingContainer = container
	cart.Categories = Categories
	assert.True(t, cart.OfferContainerContainsProductOfferingInCategory(testCategory))
	assert.True(t, cart.OfferContainerContainsProductOfferingInCategory("cat003"))
	assert.False(t, cart.OfferContainerContainsProductOfferingInCategory(testOtherCategory))
}

func TestCart_ContainsProductOfferingWithVersion(t *testing.T) {
	container := &Container{
		Items: []*CartItem{
			{
				ID:      "ProductOne",
				Version: "1.0.0",
			},
			{
				ID:      "ProductOne",
				Version: "1.1.0",
			},
		},
	}
	cart := cart([]*Container{container})

	assert.True(t, cart.ContainsProductOfferingWithVersion("ProductOne", "1.0.0"))
	assert.True(t, cart.ContainsProductOfferingWithVersion("ProductOne", "1.1.0"))
	assert.True(t, cart.ContainsProductOfferingWithVersion("ProductOne", "1.0.0", "1.1.0"))
	assert.True(t, cart.ContainsProductOfferingWithVersion("ProductOne", "1.0.0", "1.1.1"))
	assert.False(t, cart.ContainsProductOfferingWithVersion("ProductOne", "1.1.1"))
}

func TestCart_OfferContainerContainsProductOfferingWithVersion(t *testing.T) {
	container := &Container{
		Items: []*CartItem{
			{
				ID:      "ProductOne",
				Version: "1.0.0",
			},
			{
				ID:      "ProductOne",
				Version: "1.1.0",
			},
		},
	}
	cart := cart([]*Container{container})
	cart.CurrentIteratingContainer = container

	assert.True(t, cart.OfferContainerContainsProductOfferingWithVersion("ProductOne", "1.0.0"))
	assert.True(t, cart.OfferContainerContainsProductOfferingWithVersion("ProductOne", "1.1.0"))
	assert.True(t, cart.OfferContainerContainsProductOfferingWithVersion("ProductOne", "1.0.0", "1.1.0"))
	assert.True(t, cart.OfferContainerContainsProductOfferingWithVersion("ProductOne", "1.0.0", "1.1.1"))
	assert.False(t, cart.OfferContainerContainsProductOfferingWithVersion("ProductOne", "1.1.1"))
}

func cart(containers []*Container) *Cart {
	ctx := &RuleContext{
		Channel:  "",
		ExecTime: time.Time{},
	}
	entities := &Entities{
		RuleContext: ctx,
	}
	return NewCart(entities, containers)
}
