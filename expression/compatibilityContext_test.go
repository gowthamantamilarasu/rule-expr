package expression

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	subscription = "subscription"
	apple        = "apple"
	samsung      = "samsung"
	accessory    = "accessory"
)

func TestContext_AllowSingle(t *testing.T) {
	one := "galaxy"
	two := "iphone"
	ctx := NewCompatibilityContext()
	expCompatibility := map[string]bool{"galaxy#iphone": true}
	expBy := map[string]*compatibilityReference{
		one: {
			products: map[string]byte{two: 0},
			rules:    map[string]byte{"test-galaxy-iphone": 0},
		},
		two: {
			products: map[string]byte{one: 0},
			rules:    map[string]byte{"test-galaxy-iphone": 0},
		},
	}
	assert.False(t, ctx.IsSet(one, two))
	ctx.AllowCombination(two, one, "test-galaxy-iphone")
	assert.Equal(t, expCompatibility, ctx.compatible)
	assert.Equal(t, expBy, ctx.allowedBy)
	assert.Equal(t, map[string]*compatibilityReference{}, ctx.disallowedBy)
	assert.True(t, ctx.IsAllowed(one, []string{two}, Whitelist))
	assert.True(t, ctx.IsAllowed(two, []string{one}, Whitelist))
	assert.True(t, ctx.IsSet(one, two))
	assert.Empty(t, ctx.GetDisallowedByProduct(one))
	assert.Empty(t, ctx.GetDisallowedByProduct(two))
	assert.Empty(t, ctx.GetDisallowedByRule(one))
	assert.Empty(t, ctx.GetDisallowedByRule(two))
	assert.Equal(t, 1, len(ctx.GetAllowedByProduct(one)))
	assert.Equal(t, 1, len(ctx.GetAllowedByProduct(two)))
	assert.Equal(t, 1, len(ctx.GetAllowedByRule(one)))
	assert.Equal(t, 1, len(ctx.GetAllowedByRule(two)))
}

func TestContext_DisallowSingle(t *testing.T) {
	one := "galaxy"
	two := "iphone"
	ctx := NewCompatibilityContext()
	expCompatibility := map[string]bool{"galaxy#iphone": false}
	expBy := map[string]*compatibilityReference{
		one: {
			products: map[string]byte{two: 0},
			rules:    map[string]byte{"test-galaxy-iphone": 0},
		},
		two: {
			products: map[string]byte{one: 0},
			rules:    map[string]byte{"test-galaxy-iphone": 0},
		},
	}
	assert.False(t, ctx.IsSet(one, two))
	ctx.DisallowCombination(two, one, "test-galaxy-iphone")
	assert.Equal(t, expCompatibility, ctx.compatible)
	assert.Equal(t, expBy, ctx.disallowedBy)
	assert.Equal(t, map[string]*compatibilityReference{}, ctx.allowedBy)
	assert.True(t, ctx.IsDisallowed(one, []string{two}, Blacklist))
	assert.True(t, ctx.IsDisallowed(two, []string{one}, Blacklist))
	assert.True(t, ctx.IsSet(one, two))
	assert.Empty(t, ctx.GetAllowedByProduct(one))
	assert.Empty(t, ctx.GetAllowedByProduct(two))
	assert.Empty(t, ctx.GetAllowedByRule(one))
	assert.Empty(t, ctx.GetAllowedByRule(two))
	assert.Equal(t, 1, len(ctx.GetDisallowedByProduct(one)))
	assert.Equal(t, 1, len(ctx.GetDisallowedByProduct(two)))
	assert.Equal(t, 1, len(ctx.GetDisallowedByRule(one)))
	assert.Equal(t, 1, len(ctx.GetDisallowedByRule(two)))
}

func TestContext_ValidateInvalidMode(t *testing.T) {
	ctx := NewCompatibilityContext()
	errors, err := ctx.Validate([]string{}, 23794)
	assert.Empty(t, errors)
	assert.Error(t, err)
}

func TestContext_ValidWhitelistingSubscriptionDeviceAccessoryUseCase(t *testing.T) {
	ctx := NewCompatibilityContext()

	// These calls simulate the rules being executed for the current state of the cart (subscription,
	// Samsung and accessory)
	ctx.AllowStandalone(subscription, "test-standalone-subscription")
	ctx.AllowCombination(subscription, samsung, "test-subscription-samsung")
	ctx.AllowCombination(samsung, accessory, "test-samsung-accesory")

	result, err := ctx.Validate([]string{subscription, samsung, accessory}, Whitelist)
	assert.NoError(t, err)
	for _, r := range result {
		assert.True(t, r.Compatible)
	}
}

func TestContext_QualificationWhitelist(t *testing.T) {
	ctx := NewCompatibilityContext()

	// These calls simulate the rules being executed for the current state of the cart (subscription,
	// Samsung and accessory)
	ctx.AllowStandalone(subscription, "test-standalone-subscription")
	ctx.AllowCombination(subscription, samsung, "test-subscription-samsung")
	ctx.AllowCombination(samsung, accessory, "test-samsung-accesory")

	qualification, err := ctx.Qualification(map[string]byte{subscription: 0, samsung: 0}, Whitelist)
	assert.NoError(t, err)
	assert.Equal(t, []string{accessory}, qualification)
}

func TestContext_QualificationBlacklist(t *testing.T) {
	ctx := NewCompatibilityContext()

	// These calls simulate the rules being executed for the current state of the cart (subscription,
	// Samsung and accessory)
	ctx.AllowStandalone(subscription, "test-standalone-subscription")
	ctx.AllowCombination(subscription, samsung, "test-subscription-samsung")
	ctx.AllowCombination(samsung, accessory, "test-samsung-accesory")

	qualification, err := ctx.Qualification(map[string]byte{subscription: 0, samsung: 0}, Blacklist)
	assert.NoError(t, err)
	assert.Equal(t, []string{}, qualification)
}

func TestContext_QualificationInvalidMode(t *testing.T) {
	ctx := NewCompatibilityContext()

	// These calls simulate the rules being executed for the current state of the cart (subscription,
	// Samsung and accessory)
	ctx.AllowStandalone(subscription, "test-standalone-subscription")
	ctx.AllowCombination(subscription, samsung, "test-subscription-samsung")
	ctx.AllowCombination(samsung, accessory, "test-samsung-accesory")

	_, err := ctx.Qualification(map[string]byte{subscription: 0, samsung: 0}, 3)
	assert.Error(t, err)
}

func TestContext_InvalidWhitelistingSubscriptionDeviceAccessoryUseCase(t *testing.T) {
	ctx := NewCompatibilityContext()

	// These calls simulate the rules being executed for the current state of the cart (subscription,
	// Samsung and accessory)
	ctx.AllowStandalone(subscription, "test-standalone-subscription")
	ctx.AllowCombination(subscription, samsung, "test-subscription-samsung")
	ctx.AllowCombination(samsung, accessory, "test-samsung-accesory")

	// Apple isn't compatible with subscription, Samsung and accessory in this state
	result, err := ctx.Validate([]string{subscription, samsung, apple, accessory}, Whitelist)
	assert.NoError(t, err)
	errors := make([]*ValidationResult, 0)
	for _, r := range result {
		if !r.Compatible {
			errors = append(errors, r)
		}
	}
	assert.Equal(t, 1, len(errors))
}

func TestContext_ValidWBlacklistingSubscriptionDeviceAccessoryUseCase(t *testing.T) {
	ctx := NewCompatibilityContext()

	// These calls simulate the rules being executed for the current state of the cart (subscription,
	// Samsung and accessory)
	ctx.AllowStandalone(subscription, "test-standalone-subscription")
	ctx.AllowCombination(subscription, samsung, "test-subscription-samsung")
	ctx.AllowCombination(samsung, accessory, "test-samsung-accesory")

	result, err := ctx.Validate([]string{subscription, samsung, accessory}, Blacklist)
	assert.NoError(t, err)
	for _, r := range result {
		assert.True(t, r.Compatible)
	}
}

func TestContext_InvalidBlacklistingSubscriptionDeviceAccessoryUseCase(t *testing.T) {
	ctx := NewCompatibilityContext()

	// These calls simulate the rules being executed for the current state of the cart (subscription,
	// Samsung and accessory)
	ctx.DisallowCombination(apple, samsung, "test-apple-samsung")

	// Apple isn't compatible with subscription, Samsung and accessory in this state
	result, err := ctx.Validate([]string{subscription, samsung, apple, accessory}, Blacklist)
	assert.NoError(t, err)
	errors := make([]*ValidationResult, 0)
	for _, r := range result {
		if !r.Compatible {
			errors = append(errors, r)
		}
	}
	assert.Equal(t, 2, len(errors))
}

func TestContext_sanitizeCombinationSorted(t *testing.T) {
	exp := "galaxy#iphone"
	act := sanitizeCombination("galaxy", "iphone")
	assert.Equal(t, exp, act)
}

func TestContext_sanitizeCombinationUnsorted(t *testing.T) {
	exp := "galaxy#iphone"
	act := sanitizeCombination("iphone", "galaxy")
	assert.Equal(t, exp, act)
}

func TestContext_appendOrInitProductsExisting(t *testing.T) {
	m := map[string]*compatibilityReference{"one": {
		products: map[string]byte{"two": 0},
		rules:    map[string]byte{},
	}}
	exp := map[string]*compatibilityReference{"one": {
		products: map[string]byte{"two": 0, "three": 0},
		rules:    map[string]byte{},
	}}
	appendOrInitProduct(m, "one", "three")
	assert.Equal(t, exp, m)
}

func TestContext_appendOrInitNonExisting(t *testing.T) {
	m := map[string]*compatibilityReference{}
	exp := map[string]*compatibilityReference{"one": {
		products: map[string]byte{"two": 0},
		rules:    map[string]byte{},
	}}
	appendOrInitProduct(m, "one", "two")
	assert.Equal(t, exp, m)
}

func TestContext_ReasonIncompatible(t *testing.T) {
	one := "galaxy"
	two := "iphone"
	ctx := NewCompatibilityContext()
	assert.False(t, ctx.IsSet(one, two))
	ctx.DisallowCombination(two, one, "test-galaxy-iphone")
	testCases := []struct {
		name              string
		product           string
		compatible        bool
		compatibilityMode int
		expected          string
	}{
		{"Whitelist", one, false, Whitelist, "Product galaxy is not compatible with any product by default in whitelisting model."},
		{"Blacklist", one, false, Blacklist, "Product galaxy is not compatible with products [iphone], through rules [test-galaxy-iphone]."},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(tt *testing.T) {
			assert.Equal(t, tc.expected, ctx.Reason(tc.compatibilityMode, tc.product, tc.compatible))
		})
	}
}

func TestContext_AllowStandalone(t *testing.T) {
	ctx := NewCompatibilityContext()
	ctx.DisallowStandalone("my_sku", "my_rule")
	ctx.AllowStandalone("my_sku", "my_other_rule")
	assert.True(t, ctx.IsAllowed("my_sku", make([]string, 0), Whitelist))
	assert.False(t, ctx.IsAllowed("my_other_sku", make([]string, 0), Whitelist))
}

func TestContext_DisallowStandalone(t *testing.T) {
	ctx := NewCompatibilityContext()
	ctx.AllowStandalone("my_sku", "my_rule")
	ctx.DisallowStandalone("my_sku", "my_other_rule")
	assert.False(t, ctx.IsAllowed("my_sku", make([]string, 0), Blacklist))
	assert.True(t, ctx.IsAllowed("my_other_sku", make([]string, 0), Blacklist))
}
