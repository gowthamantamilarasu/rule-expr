package expression

import (
	"fmt"
	"sort"
	"strings"
)

const (
	// Whitelist approach makes everything not allowed by default, and allowed combinations have to explicitly be
	// specified through rules.
	Whitelist int = iota
	// Blacklist approach makes everything allowed by default, and not allowed combinations have to explicitly be
	// specified through rules.
	Blacklist
)

// compatibilityReference is a simple struct used to keep track of the products and rules that allowed/disallowed a
// product. These are used in the compatibility maps.
type compatibilityReference struct {
	products   map[string]byte
	rules      map[string]byte
	standAlone bool
}

// CompatibilityContext in the compatibility package refers to a compatibility context. On here are objects used to keep track of
// the compatibility result.
type CompatibilityContext struct {
	// Compatible is a map representing the compatibility result. The key is a string, a combination of both products
	// sorted alphabetically and lower cased. For instance, product "iPhone" is compatible with "Galaxy". It gets put
	// into the map separated by a hashtag character. So, galaxy#iphone gets used as key. When checking two products
	// for compatibility, they again will be sorted alphabetically and lower cased to keep this same approach going.
	// This map purely exists for fast lookups for checking if a combination is allowed.
	compatible map[string]bool
	// allowedBy is a map that keeps track of which product and rule allow the other. This map is purely used to lookup
	// which product caused the other product to be allowed. Both products are added to the map in order
	// allow lookups from both the source and target.
	allowedBy map[string]*compatibilityReference
	// disallowedBy is a map that keeps track of which product and rule disallow the other. This map is purely used to
	// lookup which product caused the other product to be disallowed. Both products are added to the map in order
	// allow lookups from both the source and target.
	disallowedBy map[string]*compatibilityReference
}

// ValidationResult is a struct used to indicate that two products (or standalone) aren't compatible with each other.
type ValidationResult struct {
	Product    string
	ByProduct  string
	ByRule     string
	Compatible bool
}

// NewCompatibilityContext creates and returns a new context object with an empty compatibility result.
func NewCompatibilityContext() *CompatibilityContext {
	return &CompatibilityContext{
		compatible:   make(map[string]bool, 0),
		allowedBy:    make(map[string]*compatibilityReference, 0),
		disallowedBy: make(map[string]*compatibilityReference, 0),
	}
}

// AllowCombination a product combination.
func (c *CompatibilityContext) AllowCombination(one, two, rule string) {
	c.setCompatible(one, two, rule, true)
}

// AllowStandalone takes one product and allows it as a standalone, not because of another product. Only the rule ID
// is linked to it.
func (c *CompatibilityContext) AllowStandalone(product, rule string) {
	if reference, ok := c.allowedBy[product]; ok {
		reference.rules[rule] = 0
		reference.standAlone = true
	} else {
		reference := &compatibilityReference{
			products: map[string]byte{},
			rules: map[string]byte{
				rule: 0,
			},
			standAlone: true,
		}
		c.allowedBy[product] = reference
	}
	delete(c.disallowedBy, product)
}

// GetAllowedByProduct will return a slice of products which indicate who allowed this product.
func (c *CompatibilityContext) GetAllowedByProduct(product string) []string {
	if prods, ok := c.allowedBy[product]; ok {
		return MapToSlice(prods.products)
	}
	return make([]string, 0)
}

// GetAllowedByRule will return a slice of rules which indicate who allowed this product.
func (c *CompatibilityContext) GetAllowedByRule(product string) []string {
	if rules, ok := c.allowedBy[product]; ok {
		return MapToSlice(rules.rules)
	}
	return make([]string, 0)
}

// DisallowCombination a product combination.
func (c *CompatibilityContext) DisallowCombination(one, two, rule string) {
	c.setCompatible(one, two, rule, false)
}

// DisllowStandalone takes one product and disallows it as a standalone, not because of another product. Only the rule
// ID is linked to it.
func (c *CompatibilityContext) DisallowStandalone(product, rule string) {
	if reference, ok := c.disallowedBy[product]; ok {
		reference.rules[rule] = 0
		reference.standAlone = true
	} else {
		reference := &compatibilityReference{
			products: map[string]byte{},
			rules: map[string]byte{
				rule: 0,
			},
			standAlone: true,
		}
		c.disallowedBy[product] = reference
	}
	delete(c.allowedBy, product)
}

// GetDisallowedByProduct will return a slice of products which indicate who disallowed this product.
func (c *CompatibilityContext) GetDisallowedByProduct(product string) []string {
	if prods, ok := c.disallowedBy[product]; ok {
		return MapToSlice(prods.products)
	}
	return make([]string, 0)
}

// GetDisallowedByRule will return a slice of rules which indicate who disallowed this product.
func (c *CompatibilityContext) GetDisallowedByRule(product string) []string {
	if rules, ok := c.disallowedBy[product]; ok {
		return MapToSlice(rules.rules)
	}
	return make([]string, 0)
}

// IsSet checks if the product combination is set. Does not check whether its allowed or disallowed; it purely checks
// if anything was set at all.
func (c *CompatibilityContext) IsSet(one, two string) bool {
	comb := sanitizeCombination(one, two)
	_, ok := c.compatible[comb]
	return ok
}

// Reason returns a reason to why the product was allowed or not allowed.
func (c *CompatibilityContext) Reason(compatibilityMode int, product string, compatible bool) string {
	if compatible {
		return c.compatibleReason(compatibilityMode, product)
	} else {
		return c.incompatibleReason(compatibilityMode, product)
	}
}

func (c *CompatibilityContext) compatibleReason(mode int, product string) string {
	// TODO: Get rid of Sprintf, and return rule/product array as object on the response. Saves about 4.8% operations per request
	if mode == Whitelist {
		reference, ok := c.allowedBy[product]
		if ok && reference.standAlone {
			return fmt.Sprintf("Product %s is compatible as standalone product, through rules %s.", product,
				c.GetAllowedByRule(product))
		}
		return fmt.Sprintf("Product %s is compatible with products %s, through rules %s.", product,
			c.GetAllowedByProduct(product), c.GetAllowedByRule(product))
	} else {
		return fmt.Sprintf("Product %s is compatible with any other product by default in blacklist mode.", product)
	}
}

func (c *CompatibilityContext) incompatibleReason(mode int, product string) string {
	// TODO: Get rid of Sprintf, and return rule/product array as object on the response. Saves about 4.8% operations per request
	if mode == Whitelist {
		reference, ok := c.disallowedBy[product]
		if ok && reference.standAlone {
			return fmt.Sprintf("Product %s is not compatible as standalone product, through rules %s.", product,
				c.GetAllowedByRule(product))
		}
		return fmt.Sprintf("Product %s is not compatible with any product by default in whitelisting model.",
			product)
	} else {
		return fmt.Sprintf("Product %s is not compatible with products %s, through rules %s.", product,
			c.GetDisallowedByProduct(product), c.GetDisallowedByRule(product))
	}
}

// Qualification returns an array which qualifies applicable products based on the provided shopping cart.
func (c *CompatibilityContext) Qualification(cartProds map[string]byte, mode int) ([]string, error) {
	if mode == Whitelist {
		return c.qualificationWhitelist(cartProds), nil
	} else if mode == Blacklist {
		return c.qualificationBlacklist(cartProds), nil
	} else {
		return []string{}, fmt.Errorf("unknown validation mode %v", mode)
	}
}

// qualificationWhitelist will read the generated compatibility maps and build a list of what is applicable
// based on what is compatible and what is not in the shopping cart.
func (c *CompatibilityContext) qualificationWhitelist(cartProds map[string]byte) []string {
	qualification := make([]string, 0)
	processed := make(map[string]byte, 0)
	for p, reference := range c.allowedBy {

		if _, cartOk := cartProds[p]; cartOk {
			continue
		}

		if reference.standAlone {
			qualification = append(qualification, p)
			continue
		}

		for qualifyP := range reference.products {
			if _, processedOk := processed[p]; !processedOk {
				if _, cartOk := cartProds[qualifyP]; cartOk {
					qualification = append(qualification, p)
					processed[p] = 0
				}
			}
		}
	}
	return qualification
}

// qualificationBlacklist will read the projection catalog and build a list of what is applicable based on this
// this projection and what is not part of the shopping cart.
func (c *CompatibilityContext) qualificationBlacklist(cartProds map[string]byte) []string {
	// TODO: Read projection database and return the whole catalog except blacklisted combinations
	return []string{}
}

// Validate function will accept a slice of products. If the slice of products - the cart - is valid, then nothing
// is returned. If there are validation errors, there are errors returned.
func (c *CompatibilityContext) Validate(products []string, mode int) ([]*ValidationResult, error) {
	if mode == Whitelist {
		return c.validateWhitelist(products), nil
	} else if mode == Blacklist {
		return c.validateBlacklist(products), nil
	} else {
		return []*ValidationResult{}, fmt.Errorf("unknown validation mode %v", mode)
	}
}

// validateWhitelist will validate the provided slice of products - representing the shopping cards - using a
// whitelisting approach. Whitelisting meaning that all products are disallowed by default, and products being
// compatible with each other need to explicitly be specified.
func (c *CompatibilityContext) validateWhitelist(products []string) []*ValidationResult {
	result := make([]*ValidationResult, 0)

	for _, cartProduct := range products {
		if !c.IsAllowed(cartProduct, products, Whitelist) {

			result = append(result, &ValidationResult{
				Product:    cartProduct,
				Compatible: false,
			})
		} else {
			result = append(result, &ValidationResult{
				Product:    cartProduct,
				Compatible: true,
			})
		}
	}

	return result
}

func (c *CompatibilityContext) IsAllowed(product string, cartProducts []string, mode int) bool {

	if mode == Whitelist {
		reference, ok := c.allowedBy[product]
		if !ok {
			return false
		}
		if reference.standAlone {
			return true
		}
		for _, cartProd := range cartProducts {
			if cartProd == product {
				continue
			}
			if _, ok := reference.products[cartProd]; ok {
				return true
			}
		}

		return false
	}

	reference, ok := c.disallowedBy[product]
	if ok {
		if reference.standAlone {
			return false
		}
		for _, cartProd := range cartProducts {
			if cartProd == product {
				continue
			}
			if _, ok := reference.products[cartProd]; ok {
				return false
			}
		}
	}

	return true
}

func (c *CompatibilityContext) IsDisallowed(product string, cartProducts []string, mode int) bool {

	if mode == Blacklist {
		reference, ok := c.disallowedBy[product]
		if !ok {
			return false
		}
		if reference.standAlone {
			return true
		}
		for _, cartProd := range cartProducts {
			if cartProd == product {
				continue
			}
			if _, ok := reference.products[cartProd]; ok {
				return true
			}
		}
		return false
	}

	reference, ok := c.allowedBy[product]
	if ok {
		if reference.standAlone {
			return false
		}
		for _, cartProd := range cartProducts {
			if cartProd == product {
				continue
			}
			if _, ok := reference.products[cartProd]; ok {
				return false
			}
		}
	}

	return true
}

// validateBlacklist will validate the provided slice of products - representing the shopping cards - using a
// blacklisting approach. Blacklisting meaning that all products are allowed by default, and products not being
// compatible with each other need to explicitly be specified.
func (c *CompatibilityContext) validateBlacklist(products []string) []*ValidationResult {
	result := make([]*ValidationResult, 0)

	for _, cartProduct := range products {
		if c.IsDisallowed(cartProduct, products, Blacklist) {

			result = append(result, &ValidationResult{
				Product:    cartProduct,
				Compatible: false,
			})
		} else {
			result = append(result, &ValidationResult{
				Product:    cartProduct,
				Compatible: true,
			})
		}
	}

	return result
}

// getCompatible will get the compatibility result. If the combination doesn't exist, false is returned. If the
// combination is not allowed, false is returned. If the combination is allowed, true is returned.
func (c *CompatibilityContext) getCompatible(one, two string) bool {
	comb := sanitizeCombination(one, two)
	if ok, comp := c.compatible[comb]; ok {
		return comp
	}
	return false
}

func MapToSlice(m map[string]byte) []string {
	r := make([]string, len(m))
	i := 0
	for k := range m {
		r[i] = k
		i++
	}
	return r
}

// sanitizeCombination will take the two arguments, sort them, lowercase them, and return them as a formatted string
// kept in the compatibility result map.
func sanitizeCombination(one, two string) string {
	prods := []string{strings.ToLower(one), strings.ToLower(two)}
	sort.Strings(prods)
	return fmt.Sprintf("%s#%s", prods[0], prods[1])
}

// setCompatible will generate a product combination and set the allowed or disallowed result based on the bool
// parameter.
func (c *CompatibilityContext) setCompatible(one, two, rule string, allowed bool) {
	c.compatible[sanitizeCombination(one, two)] = allowed
	if allowed {
		appendOrInitProduct(c.allowedBy, one, two)
		appendOrInitProduct(c.allowedBy, two, one)
		appendOrInitRule(c.allowedBy, one, rule)
		appendOrInitRule(c.allowedBy, two, rule)
	} else {
		appendOrInitProduct(c.disallowedBy, one, two)
		appendOrInitProduct(c.disallowedBy, two, one)
		appendOrInitRule(c.disallowedBy, one, rule)
		appendOrInitRule(c.disallowedBy, two, rule)
	}
}

// appendOrInitProduct will take a map[string]*compatibilityReference and a product value. If the key on the map already
// exists, then it is added to the map. If it doesn't yet exist, it's initialized and added.
func appendOrInitProduct(m map[string]*compatibilityReference, key, value string) map[string]*compatibilityReference {
	if elements, ok := m[key]; ok {
		elements.products[value] = 0
		m[key] = elements
	} else {
		m[key] = &compatibilityReference{
			products: map[string]byte{value: 0},
			rules:    map[string]byte{},
		}
	}
	return m
}

// appendOrInitRule will take a map[string]*compatibilityReference and a rule value.
func appendOrInitRule(m map[string]*compatibilityReference, key, value string) map[string]*compatibilityReference {
	elements, _ := m[key]
	elements.rules[value] = 0
	m[key] = elements
	return m
}
