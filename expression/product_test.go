package expression

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestProduct_AllowStandalone(t *testing.T) {
	ctx := context()
	tr := testRule{}
	ctx.CurrentExecutingRule = tr
	cart := make([]string, 0)
	ctx.Entities.Product.AllowStandalone()
	assert.True(t, ctx.CompatibilityContext.IsAllowed("product_one", cart, Whitelist))
	assert.True(t, ctx.CompatibilityContext.IsAllowed("product_two", cart, Whitelist))
	assert.True(t, ctx.CompatibilityContext.IsAllowed("product_three", cart, Whitelist))
	assert.False(t, ctx.CompatibilityContext.IsAllowed("product_four", cart, Whitelist))
}

func TestProduct_AllowCombination(t *testing.T) {
	ctx := context()
	tr := testRule{}
	ctx.CurrentExecutingRule = tr
	cart := []string{"product_one", "product_two"}
	ctx.Entities.Product.AllowCombinations()
	assert.True(t, ctx.CompatibilityContext.IsAllowed("product_one", cart, Whitelist))
	assert.True(t, ctx.CompatibilityContext.IsAllowed("product_two", cart, Whitelist))
	assert.True(t, ctx.CompatibilityContext.IsAllowed("product_three", cart, Whitelist))
	assert.False(t, ctx.CompatibilityContext.IsAllowed("product_four", cart, Whitelist))
}

func TestProduct_DisallowStandalone(t *testing.T) {
	ctx := context()
	tr := testRule{}
	ctx.CurrentExecutingRule = tr
	cart := make([]string, 0)
	ctx.Entities.Product.DisallowStandalone()
	assert.True(t, ctx.CompatibilityContext.IsDisallowed("product_one", cart, Blacklist))
	assert.True(t, ctx.CompatibilityContext.IsDisallowed("product_two", cart, Blacklist))
	assert.True(t, ctx.CompatibilityContext.IsDisallowed("product_three", cart, Blacklist))
	assert.False(t, ctx.CompatibilityContext.IsDisallowed("product_four", cart, Blacklist))
}

func TestProduct_DisallowCombination(t *testing.T) {
	ctx := context()
	tr := testRule{}
	ctx.CurrentExecutingRule = tr
	cart := []string{"product_one", "product_two"}
	ctx.Entities.Product.DisallowCombinations()
	assert.True(t, ctx.CompatibilityContext.IsDisallowed("product_one", cart, Blacklist))
	assert.True(t, ctx.CompatibilityContext.IsDisallowed("product_two", cart, Blacklist))
	assert.True(t, ctx.CompatibilityContext.IsDisallowed("product_three", cart, Blacklist))
	assert.False(t, ctx.CompatibilityContext.IsDisallowed("product_four", cart, Blacklist))
}

func TestProduct_AddDefaultPromo(t *testing.T) {
	ctx := context()
	tr := testRule{}
	ctx.CurrentExecutingRule = tr

	container := &Container{
		Items: []*CartItem{
			{
				ID: "product_one",
			},
			{
				ID: "product_two",
			},
			{
				ID: "product_three",
			},
		},
	}
	ctx.CurrentExecutingContainer = container

	ctx.Entities.Product.AddDefaultPromo("my_promo")
	exp := []*Promotion{
		{
			ID: "my_promo",
			Products: []string{
				"product_one",
				"product_three", // alphabetically sorted
				"product_two",
			},
		},
	}
	act := ctx.PromotionContext.DefaultPromotions()
	assert.Equal(t, exp, act)
}

func TestProduct_RemoveDefaultPromo(t *testing.T) {
	ctx := context()
	tr := testRule{}
	ctx.CurrentExecutingRule = tr
	ctx.Entities.Product.RemoveDefaultPromo("my_promo")
	exp := make([]*Promotion, 0)
	act := ctx.PromotionContext.DefaultPromotions()
	assert.Equal(t, exp, act)
}

func context() *RuleContext {

	entities := Entities{}
	product := Product{&entities, "1", "cat001", nil}
	entities.Product = &product

	ctx := &RuleContext{
		CompatibilityContext: NewCompatibilityContext(),
		PromotionContext:     NewPromotionContext(),
		Entities:             entities,
	}

	entities.RuleContext = ctx

	return ctx
}

type testRule struct{}

func (tr testRule) ID() string {
	return "test"
}

func (tr testRule) Targets() map[string]byte {
	return map[string]byte{
		"product_one":   0,
		"product_two":   0,
		"product_three": 0,
	}
}

func (tr testRule) Execute(*RuleContext, *Container) {
}
