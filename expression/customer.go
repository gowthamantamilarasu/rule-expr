package expression

// Customer object
type Customer struct {
	*Entities
	ID           string
	CustomerType string
	Status       string
	Address      *Address
	Accounts     []*Account
}

//Account to represent the customer account structure
type Account struct {
	ID            string
	AccountType   string
	Status        string
	Subscriptions []*Subscription
}

//Subscription to represent the customer subscription structure
type Subscription struct {
	ID               string
	SubscriptionType string
	Address          *Address
	Products         []*Product
}

//Address to represent the customer address structure
type Address struct {
	AddressType     string
	StreetNr        string
	StreetNrSuffix  string
	StreetName      string
	PostalCode      string
	City            string
	StateOrProvince string
	Country         string
}

// NewCustomer creates a new instance of the Customer struct and returns a pointer to it.
func NewCustomer(entities *Entities, accounts []*Account) *Customer {
	return &Customer{Entities: entities, Accounts: accounts}
}

// IsType is an expression with a variadic type argument which checks if the customer type is of at least one of the
// provided types.
func (c *Customer) IsType(customerType ...string) bool {
	for _, cusType := range customerType {
		if c.CustomerType == cusType {
			return true
		}
	}
	return false
}

// IsAccountType is an expression with a variadic type argument which checks if the customer account is of at least one
// of the specified types.
func (c *Customer) IsAccountType(accountType ...string) bool {
	for _, accType := range accountType {
		for _, account := range c.Accounts {
			if account.AccountType == accType {
				return true
			}
		}
	}
	return false
}

// HasStatus is an expression with a variadic status argument which checks if the customer has one of the provided
// statuses.
func (c *Customer) HasStatus(status ...string) bool {
	for _, customerStatus := range status {
		if c.Status == customerStatus {
			return true
		}
	}
	return false
}

//ContainsProductInCategoryId is an expression which check if the poq request contain product in category
func (c *Customer) ContainsProductInCategoryId(categoryID string) bool {
	for _, account := range c.Accounts {
		for _, sub := range account.Subscriptions {
			for _, product := range sub.Products {
				if product.Category == categoryID {
					return true
				}
			}
		}
	}
	return false
}

//HasProductInventory is an expression which check if the poq request contain productID
func (c *Customer) HasProductInventory(productID string) bool {
	for _, account := range c.Accounts {
		for _, sub := range account.Subscriptions {
			for _, product := range sub.Products {
				if product.ID == productID {
					return true
				}
			}
		}
	}
	return false
}

//HasNumberOfProductsInCategoryId is an expression which check if the poq request contain categoryID with specified number of count
func (c *Customer) HasNumberOfProductsInCategoryId(categoryID string, count int) bool {
	catCount := 0
	for _, account := range c.Accounts {
		for _, sub := range account.Subscriptions {
			for _, product := range sub.Products {
				if product.Category == categoryID {
					catCount++
				}
			}
		}
	}
	return count == catCount
}
