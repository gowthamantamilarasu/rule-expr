package expression

import "sort"

// PromotionContext struct is a struct used to keep track of the promotions currently given.
type PromotionContext struct {
	defaultPromotions map[string]map[string]byte
}

// Promotion is a struct used as representation of which promos have been added.
type Promotion struct {
	ID       string
	Products []string
}

// NewPromotionContext creates and returns a pointer to a newly created context.
func NewPromotionContext() *PromotionContext {
	return &PromotionContext{map[string]map[string]byte{}}
}

// AddDefaultPromo will take a promoID, and a map argument of products this promo should apply to.
func (c *PromotionContext) AddDefaultPromo(promoID string, cartProds, targetProducts map[string]byte) {
	if alreadyAppliedProducts, ok := c.defaultPromotions[promoID]; ok {
		for p := range targetProducts {
			if _, ok := cartProds[p]; ok {
				alreadyAppliedProducts[p] = 0
			}
		}
		return
	}

	promoProducts := make(map[string]byte, len(targetProducts))
	for p := range targetProducts {
		if _, ok := cartProds[p]; ok {
			promoProducts[p] = 0
		}
	}
	if len(promoProducts) > 0 {
		c.defaultPromotions[promoID] = promoProducts
	}
}

// RemoveDefaultPromo takes a promoID and a map argument of products which should be removed from the promo.
func (c *PromotionContext) RemoveDefaultPromo(promoID string, products map[string]byte) {
	if alreadyAppliedProducts, ok := c.defaultPromotions[promoID]; ok {
		for p := range products {
			delete(alreadyAppliedProducts, p)
		}
		if len(alreadyAppliedProducts) == 0 {
			delete(c.defaultPromotions, promoID)
		} else {
			c.defaultPromotions[promoID] = alreadyAppliedProducts
		}
	}
}

// DefaultPromotions will take all promotions registered on the context and return a slice of promotions, sorted by
// promotion ID to ensure the same order of the list.
func (c *PromotionContext) DefaultPromotions() []*Promotion {
	ret := make([]*Promotion, len(c.defaultPromotions))
	i := 0
	for promoID, promoProductMap := range c.defaultPromotions {
		ret[i] = &Promotion{
			ID:       promoID,
			Products: toSlice(promoProductMap),
		}
		i++
	}
	sort.Slice(ret, func(i, j int) bool {
		return ret[i].ID < ret[j].ID
	})
	return ret
}

// toSlice takes a map[string]byte and transforms it into a slice of strings.
func toSlice(prods map[string]byte) []string {
	ret := make([]string, len(prods))
	i := 0
	for prod := range prods {
		ret[i] = prod
		i++
	}
	sort.Slice(ret, func(i, j int) bool {
		return ret[i] < ret[j]
	})
	return ret
}
